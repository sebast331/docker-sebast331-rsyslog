# DOWNLOAD #
docker pull sebast331/rsyslog

# HOW TO RUN #
docker run -p 514:514/udp -p 514:514 -d --name rsyslog sebast331/rsyslog

# VIEW THE LOGS #
docker exec -it rsyslog bash

Logs are located in /var/log