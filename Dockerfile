FROM ubuntu

MAINTAINER Sébastien Huneault <sebast331@gmail.com>

LABEL description="Acts as a virtual rsyslog machine"
LABEL run="docker run -t -i -p 514:514/udp -p 514:514 --name rsyslog sebast331/rsyslog bash"

RUN apt-get update && apt-get install -y rsyslog

COPY rsyslog.conf /etc/

EXPOSE 514

CMD ["rsyslogd", "-n"]
